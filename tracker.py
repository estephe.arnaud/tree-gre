"""Tracker."""

import os
import pickle
import json
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import cv2
import menpo
import utils
import argparse
import preprocess
import model


MODELS = {
  "SR": model.SR,
  "RE": model.RE,
  "Soft_GRE": model.Soft_GRE,
  "Tree_GRE": model.Tree_GRE
}


class Tracker(object):
  """Tracker."""

  def __init__(self, model_dir, detector="dlib", cpu=False):
    """
    Args:
      model_dir: string. Model directory
      detector: string. Detector name in ["dlib", "opencv", "pico"]
      cpu: boolean. Use of CPU (GPU by default).
    """

    if cpu:
      os.environ["CUDA_VISIBLE_DEVICES"] = ""

    self.detector = {
      "dlib": self._load_dlib_detector(),
      "opencv": self._load_opencv_detector(),
      "pico": self._load_pico_detector()
    }[detector]

    dico = json.load(open(os.path.join(model_dir, "hparams.json"), "r"))
    params = utils.Hparams(dico)
    params.reference_shape = menpo.shape.pointcloud.PointCloud(preprocess.REFERENCE)
    self.params = params
    
    self.preprocess = lambda image: preprocess.Preprocess(params=params, image=image)
    self.graph = tf.Graph()

    self.n_step = 1
    if params.model.cascade:
      assert params.model.num_iterations
      self.n_step = params.model.num_iterations
    
    with self.graph.as_default():
      config = tf.ConfigProto()
      config.allow_soft_placement = True
      config.gpu_options.allow_growth = True

      self.session = tf.Session(config=config, graph=self.graph)

      image_shape = params.dataset.image.shape
      pts_shape = params.dataset.pts_initial.shape
      
      self.input = {
        "image": tf.placeholder(dtype=tf.float32, shape=[None] + image_shape),
        "pts_initial": tf.placeholder(dtype=tf.float32, shape=[None] + pts_shape)
      }

      model = MODELS[params.model_name]
      self.output = model.forward(self.input, params=params)
      saver = tf.train.Saver()
      ckpt = tf.train.get_checkpoint_state(params.model_dir)
      saver.restore(self.session, ckpt.model_checkpoint_path)


  def _load_dlib_detector(self):
    """Dlib detector."""

    from menpodetect.dlib import detect
    detector = detect.load_dlib_frontal_face_detector()
    return detector


  def _load_opencv_detector(self):
    """OpenCV detector."""

    from menpodetect.opencv import detect
    detector = detect.load_opencv_frontal_face_detector()
    return detector


  def _load_pico_detector(self):
    """Pico detector."""

    from menpodetect.pico import detect
    detector = detect.load_pico_frontal_face_detector()
    return detector


  def face_detection(self, image, make_detection=True):
    """Face detection.
    Args:
      image: A numpy array. Dim: (height, width, channels)

    Returns:
      A menpo.image.Image with detection in im.landmarks["bbox"].
    """

    im = menpo.image.Image(np.transpose(image, (2, 0, 1)))
    if make_detection:
      while True:
        detection = self.detector(im)
        if len(detection) != 0:
          bbox = detection[0]
          break
    else:
      bbox = self.__last_pts.bounding_box()
    
    im.landmarks["bbox"] = bbox
    return im


  def face_alignment(self, image, make_detection=True):
    """Shape prediction on image.
    Args:
      image: A numpy array. Dim: (height, width, channels)

    Returns:
      image: A numpy array. Dim: (height, width, channels)
    """

    detection = self.face_detection(image, make_detection=make_detection)
    pixels = detection.pixels.transpose((1, 2, 0))
    bbox = detection.landmarks["bbox"]
    preprocess = self.preprocess(image=pixels)
    preprocess.set_bbox(bbox)
    preprocess.get_pts_initial()
    preprocess.crop()
    preprocess.resize()
    preprocess.tfslim_preprocess()
    transform = preprocess.transform_crop.compose_after(
                preprocess.transform_resize)

    pixels = preprocess.im.pixels.transpose((1, 2, 0)).astype(np.float32)
    pts_initial = preprocess.im.landmarks["pts_initial"].as_vector().astype(np.float32)
    feed_dict = {
      self.input["image"]: np.expand_dims(pixels, 0),
      self.input["pts_initial"]: np.expand_dims(pts_initial, 0)
    }

    output = self.session.run(self.output, feed_dict=feed_dict)
    last_step = "step%d" % (self.params.model.num_iterations - 1)
    prediction = np.reshape(output["prediction"][last_step][0], (-1, 2))
    prediction = transform.apply(prediction)
    self.__last_pts = menpo.shape.pointcloud.PointCloud(prediction)
    image = utils.draw_landmarks(image, prediction)
    return image


  def live(self):
    """Tracking with local camera."""

    capture = cv2.VideoCapture(0)

    t = 0
    while True:
      _, image = capture.read()
      image = self.face_alignment(image, make_detection=(t == 0))
      cv2.imshow("Face alignment", image)
      t += 1

      if cv2.waitKey(25) & 0xFF == ord("q"):
        cv2.destroyAllWindows()
        break


if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("--model_dir", type=str, required=True)
  parser.add_argument("--detector", type=str, default="dlib")
  parser.add_argument("--cpu", action="store_true")
  parser.add_argument("--demo", action="store_true")
  parser.add_argument("--live", action="store_true")
  args = parser.parse_args()

  tracker = Tracker(
    model_dir=args.model_dir, 
    detector=args.detector,
    cpu=args.cpu
  )

  if args.demo:  
    image = mpimg.imread("./data/lenna.png")
    image = tracker.face_alignment(image)
    mpimg.imsave("./data/lenna_alignment.png", image)   

  if args.live:
    tracker.live()
