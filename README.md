# Tree-gated Deep Regressor Ensemble #

Code associated with the paper [Tree-gated Deep Regressor Ensemble For Face Alignment In The Wild](https://arxiv.org/pdf/1907.03248.pdf) 
<p align="center"> 
<img src="./data/fig_intro_detail.png" width="500">
</p>

## Getting started ##

An environment file for conda is available in the repository (environment.yml).
```sh
sudo apt-get update && sudo apt-get install cmake
conda env create -f ./environment.yml 
conda activate tree-gre
```

### Face alignment demo ###

* Input: "./data/lenna.png" 
* Output: "./data/lenna_alignment.png"

```sh
python ./tracker.py --model_dir="./weights" --demo --cpu
```

### Face alignment live with local camera ###

* GPU mode
```sh
python ./tracker.py --model_dir="./weights" --live
```

* CPU mode
```sh
python ./tracker.py --model_dir="./weights" --live --cpu
```

### Face alignment module ###

```python
from tracker import Tracker
...
tracker = Tracker(model_dir="./weights")
image_alignment = tracker.face_alignment(image)
```


## Reference ##

If you found this code useful, please cite the following paper:

	@inproceedings{arnaud2019treegre,
		title={Tree-gated Deep Regressor Ensemble For Face Alignment In The Wild},
		author={Arnaud, Est{\`e}phe and Dapogny, Arnaud and Bailly, K{\'e}vin},
		booktitle={Proceedings of the IEEE International Conference on Automatic Face and Gesture Recognition},
		year={2019}
	}

## License ##
This project is licensed under the terms of BSD 3-Clause license.
By downloading this program, you commit to comply with the license as stated in the LICENSE.md file.

## Contact ##
If you have any questions or suggestions feel free to contact me at <arnaud@isir.upmc.fr>.