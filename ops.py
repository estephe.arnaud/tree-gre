"""Ops."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import functools
import numbers
import numpy as np
import tensorflow as tf


__ACTIVATION__ = {
  "relu": tf.nn.relu,
  "softmax": tf.nn.softmax,
  "sigmoid": tf.nn.sigmoid,
  "tanh": tf.nn.tanh,
  "": None
}


class Ops(object):
  """Ops."""

  def __init__(self, training=False, kernel_initializer=tf.glorot_uniform_initializer,
               kernel_regularizer=tf.contrib.layers.l2_regularizer, use_bias=True,
               batch_norm=False, drop_rates=None, weight_decay=0.002,
               activations="relu", padding="same", conv_method="conv",
               pool_method="conv", pool_activations="", flatten=True, factor=1):

    self.activations = activations
    self.use_bias = use_bias
    self.training = training
    self.batch_norm = batch_norm
    self.drop_rates = drop_rates
    self.weight_decay = weight_decay
    self.kernel_initializer = kernel_initializer
    self.kernel_regularizer = kernel_regularizer
    self.padding = padding
    self.conv_method = conv_method
    self.pool_method = pool_method
    self.pool_activations = pool_activations
    self.flatten = flatten
    self.factor = factor


  def dense_layers(self, tensor, **kwargs):

    """Builds a stack of fully connected layers with optional dropout."""

    assert self.units

    units = self.units
    activations = self.activations
    use_bias = self.use_bias
    training = self.training
    kernel_initializer = self.kernel_initializer
    kernel_regularizer = self.kernel_regularizer
    batch_norm = self.batch_norm
    drop_rates = self.drop_rates
    weight_decay = self.weight_decay


    if drop_rates is None:
      drop_rates = [0.] * len(self.units)

    elif isinstance(drop_rates, numbers.Number):
      drop_rates = [drop_rates] * len(units)

    if not isinstance(activations, list):
      activations = [activations] * len(units)

    for i, (size, drp, act) in enumerate(zip(units, drop_rates, activations)):
      activation = __ACTIVATION__[act]

      with tf.variable_scope("dense_block_%d" % i):
        if drp:
          tensor = self.dropout(tensor, drp, training)
        tensor = tf.layers.dense(
          tensor, size, use_bias=use_bias,
          bias_initializer=tf.zeros_initializer(),
          kernel_initializer=kernel_initializer(),
          kernel_regularizer=kernel_regularizer(weight_decay),
          **kwargs
        )

        if activation:
          if batch_norm:
            tensor = self.batch_normalization(tensor, training=training)
          tensor = activation(tensor)

    return tensor


  def convolutional_layers(self, tensor, **kwargs):

    """Builds a stack of convolutional layers with dropout and pooling."""

    assert self.fmaps
    assert self.kernels
    assert self.strides
    assert self.pool_sizes
    assert self.pool_strides

    training = self.training
    kernel_initializer = self.kernel_initializer
    kernel_regularizer = self.kernel_regularizer
    batch_norm = self.batch_norm
    drop_rates = self.drop_rates
    weight_decay = self.weight_decay

    activations = self.activations
    use_bias = self.use_bias
    fmaps = self.fmaps
    kernels = self.kernels
    strides = self.strides
    pool_sizes = self.pool_sizes
    pool_strides = self.pool_strides
    padding = self.padding
    conv_method = self.conv_method
    pool_method = self.pool_method
    pool_activations = self.pool_activations
    flatten = self.flatten
    factor = self.factor

    if len(self.get_shape(tensor)) == 5:
      tensor = tf.reshape(
        tensor,
        (-1,
         self.get_shape(tensor)[-3],
         self.get_shape(tensor)[-2],
         self.get_shape(tensor)[-1]
        )
      )

    else:
      assert len(self.get_shape(tensor)) < 5

    if pool_sizes is None:
      pool_sizes = [1] * len(fmaps)
    if pool_strides is None:
      pool_strides = pool_sizes
    if strides is None:
      strides = [1] * len(fmaps)
    if drop_rates is None:
      drop_rates = [0.] * len(fmaps)
    elif isinstance(drop_rates, numbers.Number):
      drop_rates = [drop_rates] * len(fmaps)

    if not isinstance(activations, list):
      activations = [activations] * len(fmaps)
    if not isinstance(pool_activations, list):
      pool_activations = [pool_activations] * len(fmaps)

    if conv_method == "conv":
      conv = functools.partial(
        tf.layers.conv2d,
        kernel_initializer=kernel_initializer(),
        kernel_regularizer=kernel_regularizer(weight_decay)
      )
    elif conv_method == "transposed":
      conv = functools.partial(
        tf.layers.conv2d_transpose,
        kernel_initializer=kernel_initializer(),
        kernel_regularizer=kernel_regularizer(weight_decay)
      )
    elif conv_method == "separable":
      conv = functools.partial(
        tf.layers.separable_conv2d,
        depthwise_initializer=kernel_initializer(),
        pointwise_initializer=kernel_initializer(),
        depthwise_regularizer=kernel_regularizer(weight_decay),
        pointwise_regularizer=kernel_regularizer(weight_decay)
      )

    for i, (fs, ks, ss, pz, pr, drp, act, pool_act) in enumerate(
      zip(fmaps, kernels, strides, pool_sizes, pool_strides,
          drop_rates, activations, pool_activations)):

      activation = __ACTIVATION__[act]
      pool_activation = __ACTIVATION__[pool_act]

      with tf.variable_scope("conv_block_%d" % i):
        if drp:
          tensor = self.dropout(tensor, drp, training)

        if ks > 0 and ss > 0:
          tensor = conv(
            tensor, fs, ks, ss, padding, use_bias=use_bias, name="conv2d", **kwargs)
          if activation:
            if batch_norm:
              tensor = self.batch_normalization(tensor, training=training)
            tensor = activation(tensor)

        if pz > 1 and pr > 0:
          if pool_method == "max":
            tensor = tf.layers.max_pooling2d(
              tensor, pz, pr, padding, name="max_pool")
          elif pool_method == "std":
            tensor = tf.space_to_depth(tensor, pz, name="space_to_depth")
          elif pool_method == "dts":
            tensor = tf.depth_to_space(tensor, pz, name="depth_to_space")
          else:
            tensor = conv(
              tensor, fs, pz, pr, padding, use_bias=use_bias,
              name="strided_conv2d", **kwargs)
            if pool_activation:
              if batch_norm:
                tensor = self.batch_normalization(tensor, training=training)
              tensor = pool_activation(tensor)

    if flatten:
      num_features = np.prod(self.get_shape(tensor)[1:])
      units = factor * num_features
      tensor = tf.reshape(tensor, (-1, units))

    return tensor


  def get_mu(self, decision):
    batch_size, n_leaves = self.get_shape(decision)
    depth = int(np.log2(n_leaves))

    decision = tf.expand_dims(decision, 2)
    decision_comp = 1 - decision
    decision = tf.concat([decision, decision_comp], 2)

    _mu = tf.ones((batch_size, 1, 1))

    begin = 1
    end = 2
    for d in range(0, depth):
      _mu = tf.reshape(_mu, (batch_size, -1, 1))
      _mu = tf.tile(_mu, (1, 1, 2))
      _decision = decision[:, begin:end, :]
      _mu *= _decision
      begin = end
      end = begin + 2 ** (d + 1)

    mu = tf.reshape(_mu, (batch_size, n_leaves))
    return mu


  def tree_op(self, decision, leaves):

    """Neural tree op."""

    batch_size, n_leaves, n_units = self.get_shape(leaves)

    mu = self.get_mu(decision)
    mu = tf.expand_dims(mu, 2)
    mu = tf.tile(mu, (1, 1, n_units))

    w_output = tf.multiply(mu, leaves)
    output = tf.reduce_sum(w_output, 1)

    return output


  def get_shape(self, tensor):

    """Returns static shape if available and dynamic shape otherwise."""

    static_shape = tensor.shape.as_list()
    dynamic_shape = tf.unstack(tf.shape(tensor))
    dims = [
      s[1] if s[0] is None else s[0]
      for s in zip(static_shape, dynamic_shape)
    ]
    return dims


  def extract_patches(self, images, shapes, num_patches, patch_shape):

    """Extracts patches from shape.
    Returns:
      Tensor [batch_size, num_patches, patch_shape[0], patch_shape[1], num_channels]
    """

    patches = []
    size = tf.constant(patch_shape)
    for i in range(num_patches):
      offsets = tf.gather(shapes, [2*i, 2*i + 1], axis=1)
      patch = tf.image.extract_glimpse(
        images,
        size=size,
        offsets=offsets,
        centered=False,
        normalized=False
      )
      patches.append(patch)

    patches = tf.stack(patches, 1)
    return patches


  def dropout(self, tensor, dropout, training):

    """Dropout."""

    def true(tensor=tensor, dropout=dropout):
      tensor = tf.layers.dropout(tensor, dropout, training=True)
      return tensor

    def false():
      return tensor

    return self.tf_cond(training, true, false)


  def batch_normalization(self, tensor, training, epsilon=0.001, momentum=0.9,
                          name=None):

    """Performs batch normalization."""

    with tf.variable_scope(name, default_name="batch_normalization"):
      channels = tensor.shape.as_list()[-1]
      axes = list(range(tensor.shape.ndims - 1))

      beta = tf.get_variable(
        "beta", channels, initializer=tf.zeros_initializer(), trainable=True)
      gamma = tf.get_variable(
        "gamma", channels, initializer=tf.ones_initializer(), trainable=True)

      avg_mean = tf.get_variable(
        "avg_mean", channels, initializer=tf.zeros_initializer(),
        trainable=False)
      avg_variance = tf.get_variable(
        "avg_variance", channels, initializer=tf.ones_initializer(),
        trainable=False)

      true = lambda : tf.nn.moments(tensor, axes=axes)
      false = lambda : (avg_mean, avg_variance)
      mean, variance = self.tf_cond(training, true, false)

      tensor = tf.nn.batch_normalization(
          tensor, mean, variance, beta, gamma, epsilon)

      def true():
        update_mean = tf.assign(
          avg_mean,
          avg_mean * momentum + mean * (1.0 - momentum)
        )

        update_variance = tf.assign(
          avg_variance,
          avg_variance * momentum + variance * (1.0 - momentum)
        )

        tf.add_to_collection(tf.GraphKeys.UPDATE_OPS, update_mean)
        tf.add_to_collection(tf.GraphKeys.UPDATE_OPS, update_variance)
        return 1

      def false():
        return -1

      update = self.tf_cond(training, true, false)

    return tensor


  def tf_cond(self, training, true, false):
    if type(training) != tf.Tensor:
      return true() if training else false()
    return tf.cond(training, true, false)
