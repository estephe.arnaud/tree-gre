"""Model."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
import ops as module_ops


def forward(input, params):
  """Forward."""

  num_landmarks = params.preprocess.image.num_landmarks
  size = (-1, num_landmarks, 2)
  delta = tf.zeros_like(input["pts_initial"])

  output = {}
  output["prediction"] = {}

  fmaps = lambda K: [
    K * n for i, n in enumerate(params.model.features.fmaps)
    if i < len(params.model.features.fmaps) - 1
  ] + [params.model.features.fmaps[-1]]

  for step in range(params.model.num_iterations):
    ops = module_ops.Ops(training=False)
    ops.batch_norm = params.model.batch_norm
    batch_size = ops.get_shape(input["image"])[0]

    with tf.variable_scope("step%d" % step, reuse=False):
      current = input["pts_initial"] + delta

      with tf.variable_scope("features"):
        ops.fmaps = fmaps(params.model.features.K)
        ops.kernels = params.model.features.kernels
        ops.strides = params.model.features.strides
        ops.pool_method = params.model.features.pool_method
        ops.pool_sizes = params.model.features.pool_sizes
        ops.pool_strides = params.model.features.pool_strides
        ops.padding = params.model.features.padding
        ops.flatten = True
        ops.factor = num_landmarks

        patches = ops.extract_patches(
          images=input["image"],
          shapes=current,
          num_patches=num_landmarks,
          patch_shape=params.model.features.patch_shape
        ) if params.model.features.usePatches else input["image"]

        features = ops.convolutional_layers(patches)

        if params.model.features.hidden_units:
          hidden_units = params.model.features.hidden_units
          if not isinstance(hidden_units, list):
            hidden_units = [hidden_units]
          ops.units = hidden_units
          features = ops.dense_layers(features)

      with tf.variable_scope("predictor"):
        hidden_units = params.model.predictor.hidden_units
        if not isinstance(hidden_units, list):
          hidden_units = [hidden_units]

        ops.units = hidden_units + [2 * num_landmarks]
        ops.activations = ["relu"] * len(hidden_units) + [""]
        ds_prediction = ops.dense_layers(features)

      delta = tf.add(delta, ds_prediction)
      prediction_shape = tf.add(input["pts_initial"], delta, name="prediction")
      output["prediction"]["step%d" % step] = prediction_shape

  return output
